<?php

namespace Drupal\xmas_registration\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Exception;

class RegistrationForm extends FormBase
{

  /**
   * @var mixed
   */
  private $department;


  /**
   * @return string
   */
  public function getFormId()
  {
    return 'xmas_party_form';
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @param null $department
   * @return array
   */
  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @param null $department
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $department = null)
  {
    $this->messenger = Drupal::messenger(); // @ TODO Use constructor duh

    $this->department = $department;


    $form['employee_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Employee Name'),
      '#required' => TRUE,
    ];

    $form['one_plus'] = [
      '#type' => 'radios',
      '#title' => $this->t('One Plus'),
      '#required' => TRUE,
      '#default_value' => 0,
      '#options' => array(
        1 => $this
          ->t('Yes'),
        0 => $this
          ->t('No')
      ),
    ];

    $form['kids_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of Kids'),
      '#required' => TRUE,
    ];

    $form['vegetarians_amount'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of Vegetarians'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#description' => $this->t('Please enter a valid email address'),
      '#required' => TRUE,
    ];


    // ACTIONS

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;

  }

  //OOTB Validations

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $created_date = date('d-m-Y');


    try {
      $newRegistration = Node::create([
        'type' => 'registration',
        'title' => $form_state->getValue('employee_name'),
        'field_one_plus' => $form_state->getValue('one_plus'),
        'field_kids_amount' => $form_state->getValue('kids_amount'),
        'field_vegetarians_amount' => $form_state->getValue('vegetarians_amount'),
        'field_email' => $form_state->getValue('email'),
        'field_department' => $this->department, // @TODO Should be replaced with another method
        'created_date' => $created_date
      ])->save();


      $this->messenger->addMessage('Your registration was successful.', $this->messenger::TYPE_STATUS);
    } catch (Exception $e) {
      $this->messenger->addMessage('Your registration failed.', $this->messenger::TYPE_ERROR);
    }

  }
}
